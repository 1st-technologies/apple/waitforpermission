# WaitForPermission

WaitForPermission is an auxiliary script for Jamf to pause the execution of policies until a permission has been applied.

## Usage

Add this script to your policy install script and configure to run **after** other actions.

![Parameters](images/image1.png)

### Parameters

* **Bundle ID** - Specifies the plist file to monitor.
* **Permission Code** -  Specifies the field in the plist file to monitor.
* **Maximum attempts** - Specifies the number times to attempt to test for the correct plist file value. Optional if no non-default sleep setting is defined, default 300
* **Time to sleep** - The number of seconds. Optional, default 3 seconds.

The default sleep time out of 3 seconds and 300 attempts results i a default timeout of 15 minutes.

### Permission Codes

A partial list of macOS service codes for Privacy & Security options; The remainder can be set via configuration profiles.

* **kTCCServiceBluetoothAlways** - Bluetooth
* **kTCCServiceCamera** - Camera
* **kTCCServiceLocation** - Location
* **kTCCServiceMicrophone** - Microphone
* **kTCCServiceScreenCapture** - Screen capture and recording
* **kTCCServiceAccessibility** - Accessibility
* **kTCCServicePostEvent** - Send keystrokes
* **kTCCServiceListenEvent** - Monitor input from the keyboard
