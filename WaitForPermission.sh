#!/bin/bash

installDone="false"
bundleId="$4"
permission="$5"
sleepTime="$6"
maxAttempts="$7"

if [ -z "$bundleId" ] || [ -z "$permission" ]; then
    echo "Missing Bundle ID or Permission parameter, quitting."
    exit 1
fi

if [ -z "$sleepTime" ]; then
    sleepTime=3
fi

if [ -z "$maxAttempts" ]; then
    maxAttempts=300
fi

currentAttempt=1

while [ $installDone = "false" ] && [ $currentAttempt -le "$maxAttempts" ]; do
    echo "Waiting for $bundleId to be granted $permission..."
    result=$(sqlite3 "/Library/Application Support/com.apple.TCC/TCC.db" "select auth_value from access where client = \"$bundleId\" and service = \"$permission\"")

    if [ "$result" = "2" ]; then
        installDone="true"
    fi

    (( currentAttempt++ ))
    sleep "$sleepTime"
done